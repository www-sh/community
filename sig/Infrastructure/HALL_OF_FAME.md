# Hall of Fame

The file is used to record people who made contributions to Infrastrusture and had left for a while.

### Historical Contributors
| gitee_id | name | start_time | left_time |
| :---: | :---: | :---: | :---: |
| dogsheng | 惊奇脆片饼干 | 2019-12-30 | 2021-09-30 |
| freesky-edward | freesky-edward | 2019-12-30 | 2021-09-30 |

Thanks for being with Infrastrusture all the way.
